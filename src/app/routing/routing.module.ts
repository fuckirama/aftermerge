import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//import { AuthGuard } from '../auth.guard';
//components
import { AppComponent } from '../app.component';
import { AppNavbarComponent } from '../app-navbar/app-navbar.component';
import { HomeveriComponent } from '../homeveri/homeveri.component';


const routes: Routes = [
 { path: '', component: HomeveriComponent, }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class RoutingModule { }
