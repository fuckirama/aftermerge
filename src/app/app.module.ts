import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, JsonpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { AppNavbarComponent } from './app-navbar/app-navbar.component';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { HomeveriComponent } from './homeveri/homeveri.component';
import { FooterComponent } from './footer/footer.component';

//firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../environments/environment';
//services
import { AuthService } from './services/auth.service';
//modules
import { RoutingModule } from './routing/routing.module';




@NgModule({
  declarations: [
    AppComponent,
    AppNavbarComponent,
    HomeveriComponent,
    FooterComponent,
    
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    HttpModule,
    JsonpModule,
    FormsModule,
    RoutingModule

  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { 

  

}
