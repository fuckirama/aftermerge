import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeveriComponent } from './homeveri.component';

describe('HomeveriComponent', () => {
  let component: HomeveriComponent;
  let fixture: ComponentFixture<HomeveriComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeveriComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeveriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
